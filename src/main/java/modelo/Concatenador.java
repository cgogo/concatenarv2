/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author cGogo
 */
public class Concatenador {
    private String palabra1;
    private String palabra2;
    private String resultado;

    /**
     * @return the palabra1
     */
    public String getPalabra1() {
        return palabra1;
    }

    /**
     * @param palabra1 the palabra1 to set
     */
    public void setPalabra1(String palabra1) {
        this.palabra1 = palabra1;
    }

    /**
     * @return the palabra2
     */
    public String getPalabra2() {
        return palabra2;
    }

    /**
     * @param palabra2 the palabra2 to set
     */
    public void setPalabra2(String palabra2) {
        this.palabra2 = palabra2;
    }

    /**
     * @return the resultado
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
    

    public void concatenar(String palabra1, String palabra2){
        this.resultado= palabra1 + " " +palabra2;
    }
    
}
